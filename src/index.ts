import fetch from 'cross-fetch'
import { db } from '~/jsondb'

export const main = async () => {
	const r = await fetch('https://yesno.wtf/api').then(r => r.json())
	console.log('NODE_ENV=', process.env.NODE_ENV)
	console.log('hoge', r)
	await db.users.create({ name: 'hoge' + Date.now() })
	console.log(await db.users.findMany())
}

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
	if ('undefined' === typeof process) return
	process.exit(1)
})

import { promises as fs } from 'fs'
import path from 'path'

export interface Repo<Item extends { id: number }> {
	findMany(): Promise<Item[]>
	findOne(): Promise<Item | null>
	create(init: Omit<Item, 'id'>): Promise<Item>
	update(data: Partial<Item>): Promise<{ count: number }>
	remove(data: Pick<Item, 'id'>): Promise<{ count: number }>
}

type JsonContent<Item extends { id: number }> = {
	list: Item[]
	lastId: Item['id']
}

class RepoImpl<Item extends { id: number }> implements Repo<Item> {
	constructor(private readonly filepath: string) {}
	private async _readJson(): Promise<JsonContent<Item>> {
		return await fs
			.readFile(this.filepath, 'utf-8')
			.then(txt => JSON.parse(txt))
			.catch(x =>
				'ENOENT' === x.code ? { list: [], lastId: 0 } : Promise.reject(x),
			)
	}
	private async _writeJson(v: JsonContent<Item>) {
		await fs.writeFile(this.filepath, JSON.stringify(v))
	}
	async findMany(): Promise<Item[]> {
		const { list } = await this._readJson()
		return list
	}
	async findOne(): Promise<Item | null> {
		const { list } = await this._readJson()
		return list[0] || null
	}
	async create(init: Omit<Item, 'id'>): Promise<Item> {
		const { list, lastId } = await this._readJson()
		const newItem = { ...init, id: lastId + 1 } as Item
		list.push(newItem)
		await this._writeJson({ list, lastId: newItem.id })
		return newItem
	}
	async update(data: Partial<Item>): Promise<{ count: number }> {
		const { list, ...rest } = await this._readJson()
		let count = 0
		const newlist = list.map(i =>
			i.id === data.id ? (++count, { ...i, ...data }) : i,
		)
		await this._writeJson({ ...rest, list: newlist })
		return { count }
	}
	async remove(data: Pick<Item, 'id'>): Promise<{ count: number }> {
		const { list, ...rest } = await this._readJson()
		let count = 0
		const newlist = list.filter(i => (i.id === data.id ? (++count, 0) : 1))
		await this._writeJson({ ...rest, list: newlist })
		return { count }
	}
}

export type User = {
	id: number
	name: string
}

export const db = {
	users: new RepoImpl<User>(path.join(__dirname, 'db.json')),
}
